#include "../include/debug.h"

// Exercise 5-10. Write the program expr, which evaluates a reverse Polish
// expression from the command line, where each operator or operand is a
// separate argument. For example,

//     expr 2 3 4 + *
//     evaluates 2 * (3+4).

// static double **val;
// static char **op;

double atof(const char *);

int main(int argc, char **argv) {
  /* argv is an array of pointer to char */
  int result;
  double *val;
  double op1, op2;
  ++argv;
  for (; *argv != NULL && argc > 0; argv++, --argc) {
    switch (**argv) {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      // log("NUMBER");
      // dprints(*argv);
      *val = atof(*argv);
      // dprintd(*val);
      val++;
      break;
    case '+':
      // log("OPERATOR");
      // dprints(*argv);
      *val = *--val + *--val;
      // dprintd(*val);
      val++;
      break;
    case '/':
      // log("OPERATOR");
      // dprints(*argv);
      op1 = *--val;
      op2 = *--val;
      // dprintd(op1);
      // dprintd(op2);
      *val = op2 / op1;
      // dprintd(*val);
      val++;
      break;
    case '-':
      // log("OPERATOR");
      // dprints(*argv);
      op1 = *--val;
      op2 = *--val;
      // dprintd(op1);
      // dprintd(op2);
      *val = op2 - op1;
      // dprintd(*val);
      val++;
      break;
    case '*':
      // log("OPERATOR");
      // dprints(*argv);
      *val = *--val * *--val;
      // dprintd(*val);
      val++;
      break;
    }
  }
  double ans = *--val;
  dprintd(ans);
}
