#include "../include/chapter2.h"
#include "../include/chapter3.h"
#include "../include/chapter4.h"
#include "../include/chapter5.h"
#include "../include/chapter6.h"
#include "../include/chapter8.h"
#include "../include/debug.h"
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

void run_chapter2();
void run_chapter3();
void run_chapter4();
void run_chapter5(void);
void run_chapter6(int argc, char *argv[]);
void run_chapter7(int argc, char *argv[]);
void run_chapter8(int argc, char *argv[]);

int main(int argc, char *argv[]) {
  run_chapter2();
  run_chapter3();
  run_chapter4();
  run_chapter5();
  run_chapter6(argc, argv);
  run_chapter7(argc, argv);
  run_chapter8(argc, argv);
  return 0;
}

void run_chapter2() {
  printf("Chapter 2 solutions:\n");
  char s[] = "test";
  char s2[] = "abcdef";
  printf("s1: %s, s2: %s\n", s, s2);
  squeeze2(s, s2);

  printf("squeeze(%s,%s): %s\n", "test", s2, s);
  printf("any(%s, %s) = %i\n", s, s2, any(s, s2));

  printf("getbits(0157,7,3) = %i\n", getbits(0157, 7, 3));
  printf("getbits(0137,3,3) = %i\n", getbits(0137, 3, 3));

  printf("setbits(0151,7,3,0137) = %i\n", setbits(0151, 7, 3, 0137));
  printf("setbits(0751,7,3,0137) = %i\n", setbits(0751, 7, 3, 0137));

  printf("invert(0751,7,3) = %i\n", invert(0751, 7, 3));

  printf("rightrot(%i,%i) = %i\n", 3, 1, rightrot(3, 1));

  printf("bitcount(077) = %i\n", bitcount(077));
  printf("bitcount(01) = %i\n", bitcount(01));
}

void run_chapter3() {
  printf("Chapter 3 solutions:\n");
  char s[100];
  char t[] = "test \t this \n";
  escape(s, t);
  printf("escape = %s\n", s);

  char s1[] = "a-z";
  char s2[62];
  expand(s1, s2);
  printf("expand = %s\n", s2);

  char s3[] = "a-b-c";
  char s4[62];
  expand(s3, s4);
  printf("expand = %s\n", s4);

  char s5[] = "a-z0-9";
  char s6[62];
  expand(s5, s6);
  printf("expand = %s\n", s6);

  char s7[] = "-a-z";
  char s8[62];
  expand(s7, s8);
  printf("expand = %s\n", s8);

  char s9[100];
  itoa(INT_MIN, s9);
  printf("itoa(%i, s9) = %s\n", INT_MIN, s9);

  char s10[100];
  itoa(INT_MAX, s10);
  printf("itoa(%i, s10) = %s\n", INT_MAX, s10);

  char s11[100];
  itob(INT_MAX, s11, 5);
  printf("itob(%i, s11, 5) = %s\n", INT_MAX, s11);

  char s12[100];
  itob(300, s12, 16);
  printf("itob(%i, s10, 16) = %s\n", 300, s12);

  char s13[100];
  itoa2(INT_MAX, s13, 40);
  printf("itoa2(%i, s13, 40) = %s\n", INT_MAX, s13);
}

void run_chapter4() {
  printf("Chapter 4 solutions:\n");
  char s[] = "Could should would";
  char t[] = "ould";
  printf("strindex(%s,%s) = %i\n", s, t, strindex(s, t));

  printf("atof(%s) = %f\n", "123.45e-6", atof2("123.45e-6"));
  printf("atof(%s) = %f\n", "123.45e-16", atof2("123.45e-16"));
  printf("atof(%s) = %f\n", "123.45e6", atof2("123.45e6"));
  printf("atof(%s) = %f\n", "123.45e16", atof2("123.45e16"));

  // calc();
  // calc_by_line();

  char s2[100];
  itoa_recursive(123, s2, 0);
  printf("itoa_recursive(%i, s2, 0) = %s\n", 123, s2);

  char s2_2[100];
  itoa_recursive(12345, s2_2, 0);
  printf("itoa_recursive(%i, s2_2, 0) = %s\n", 12345, s2_2);

  char s3[] = "abcdef";
  reverse_recursive(s3, 0, 0);
  printf("reverse_recursive(abcdef) = %s\n", s3);

  char s4[] = "abcdefg";
  reverse_recursive(s4, 0, 0);
  printf("reverse_recursive(abcdefg) = %s\n", s4);

  int a = 5;
  int b = 6;
  swap(int, a, b);
  dprint(a);
  dprint(b);
}

void run_chapter5(void) {
  my_log("Running chapter 5");

  char *s = (char *)malloc(sizeof(char));
  int i = 56;
  itoa_ptr(&i, s);
  dprints(s);
}

void run_chapter6(int argc, char *argv[]) {
  my_log("Running chapter 6");
  // count_keywords();
  // count_grouped_variable_names(argc, argv);
  // cross_referencer(argc, argv);
  // distinct_words(argc, argv);

  // install("test", "foo");
  // install("testtesp", "something else");

  // undef("test");
  // undef("test");

  // print_nlist(86);

  // undef("testtesp");
  // print_nlist(86);

  define_processor();
}

void run_chapter7(int argc, char *argv[]) {
  my_log("Running chapter 7");
  argv[0] = "./lower";
  // casing(argc, argv);

  // minprintf("%-15.2s foo %-15d thing \n", "test", 31);
  // char str[MAXWORD];
  // int i;
  // double d;
  // minscanf("%s foo % %d %e", str, &i, &d);
  // dprints(str);
  // dprint(i);
  // dprintd(d);

  char *args[] = {"cat", "main.c"};

  // cat(2, args);

  diff("main.c", "chapter7.c"); /* Exercise 7-6 */
}

void run_chapter8(int argc, char *argv[]) {
  my_log("run_chapter8");
  // my_cat(argc, argv);
  MY_FILE *fp;
  char c;

  if ((fp = my_fopen("chapter9.c", "r")) == NULL) {
    printf("Couldn't open chapter9.c\n");
  }
  if ((fp = my_fopen("chapter8.c", "r")) == NULL) {
    printf("Couldn't open chapter8.c\n");
  } else {
      c = my_getc(fp);
      printf("getc returned: %c\n", c);
  }
  if ((fp = my_fopen("chapter9.c", "w")) == NULL) {
    printf("Couldn't open chapter9.c\n");
  } else {
      c = my_putc('a', fp);
      c = my_putc('b', fp);
      printf("putc returned: %c\n", c);
      my_fclose(fp);
      // my_fflush(fp);
      char *args[2] = {"cat", "chapter9.c"};
      my_cat(2, args);
  }

  /* test fsize */
  printf("\n");
  fsize(".");
}
