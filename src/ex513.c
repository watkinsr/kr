#include "../include/debug.h"
#include <stdio.h>
#include <string.h>
#define MAXLINE 1000

#define MAXLINES 5000    /* max #lines to be sorted */
#define ALLOCSIZE 10000  /* size of available space */
char *lineptr[MAXLINES]; /* pointers to text lines */

int my_getline(char *line, int max);
double atof(const char *);
int readlines(char *lineptr[], int maxlines);

/* find: print lines that match pattern from 1st arg */
int main(int argc, char *argv[]) {
  int amount;
  char **p;
  int i;

  char line[MAXLINE];
  long lineno = 0;
  int c;

  while (--argc > 0 && (*++argv)[0] == '-') {
    while ((c = *++argv[0])) {
      switch (c) {
      case 'n':
        if (*++argv != NULL) {
          amount = atof(*argv);
          --argv;
        } else {
          argc = -1; /* Error */
        }
        break;
      default:
        printf("tail: illegal option %c\n", c);
        argc = 0;
        break;
      }
      break;
    }
  }
  if (argc <= 0)
    printf("Usage: tail -n AMOUNT\n");
  else {
    int nlines;
    nlines = readlines(lineptr, MAXLINES);
    for (i = amount < nlines ? nlines - amount : 0; i < nlines; i++) {
      printf("%s\n", *(lineptr + i));
    }
  }
}

/* my_getline:  get line into s, return length */
int my_getline(char *s, int lim) {
  int c, i;

  i = 0;
  while (--lim > 0 && (c = getchar()) != EOF && c != '\n') {
    i++;
    *s++ = c;
  }
  if (c == '\n') {
    i++;
    *s++ = c;
  }
  *s = '\0';
  return i;
}

#define MAXLEN 1000 /* max length of any input line */
int my_getline(char *, int);
char *alloc(int);

static char allocbuf[ALLOCSIZE]; /* storage for alloc */
static char *allocp = allocbuf;  /* next free position */

char *alloc(int n) /* return pointer to n characters */
{
  if (allocbuf + ALLOCSIZE - allocp >= n) { /* it fits */
    allocp += n;
    return allocp - n; /* old p */
  } else               /* not enough room */
    return 0;
}

/* readlines:  read input lines */
int readlines(char *lineptr[], int maxlines) {
  int len, nlines;
  char *p, line[MAXLEN];

  nlines = 0;
  while ((len = my_getline(line, MAXLEN)) > 0)
    if (nlines >= maxlines || (p = alloc(len)) == NULL)
      return -1;
    else {
      line[len - 1] = '\0'; /* delete newline */
      strcpy(p, line);
      lineptr[nlines++] = p;
    }
  return nlines;
}
