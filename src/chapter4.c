#include "../include/chapter4.h"
#include "../include/debug.h"
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int sp = 0;
double val[MAXVAL];

static char buf[BUFSIZE]; /*  buffer for ungetch */
static int bufp = 0;      /*  next free position in buf */

size_t bufsize = 32;
char *buffer;
int linep = 0;

/* Variable array a-z and last index is most recent (ANS) */
double vars[27];

/* Exercise 4-1. Write the function strindex(s,t) which returns the position of
 * the rightmost */
/* occurrence of t in s , or -1 if there is none. */

/* strindex: return index of t in s, -1 if none */
int strindex(char s[], char t[]) {
  int rightmost = -1;
  int i, j, k;
  for (i = 0; s[i] != '\0'; i++) {
    /* printf("i = %i\n", i); */
    for (j = i, k = 0; t[k] != '\0' && s[j] == t[k]; j++, k++)
      ;
    if (k > 0 && t[k] == '\0') {
      rightmost = i;
    }
  }
  return rightmost;
}

/* Exercise 4-2. Extend atof to handle scientific notation of the form */
/* 123.45e-6 */
/* where a floating-point number may be followed by e or E and an optionally
 * signed exponent. */
/* atof: convert string s to double */
double atof2(char s[]) {
  double val, power, tmp;
  int i, sign;
  for (i = 0; isspace(s[i]); i++) /* skip white space */
    ;
  sign = (s[i] == '-') ? -1 : 1;

  if (s[i] == '+' || s[i] == '-')
    i++;
  for (val = 0.0; isdigit(s[i]); i++)
    val = 10.0 * val + (s[i] - '0');

  /* e-notation could be found in the integer or the fraction section */
  tmp = val;
  val = check_exp(s, i, tmp);
  if (tmp != val)
    return val;

  if (s[i] == '.')
    i++;
  for (power = 1.0; isdigit(s[i]); i++) {
    val = 10.0 * val + (s[i] - '0');
    power *= 10;
  }

  return sign * check_exp(s, i, val / power);
}

double check_exp(char s[], int i, double val) {
  int exp = 0;
  int sign = 1;
  int j;
  /* printf("check_exp(%s,%i,%f)\n", s,i,val); */
  if (s[i] == 'e') { /* Inside the integer section */
    /* printf("found exponent\n"); */
    i++;
    if (s[i] == '-') {
      sign = -1;
      i++;
      for (; s[i] != '\0'; i++)
        exp = exp * 10 + (s[i] - '0');
      printf("exp = %i\n", exp);
    } else {
      for (; s[i] != '\0'; i++)
        exp = exp * 10 + (s[i] - '0');
    }
  }
  for (j = 0; j < exp; j++)
    if (sign > 0)
      val = val * 10;
    else
      val = val / 10;
  return val;
}

int calc_by_line() {
  int type;
  char s[MAXOP];
  buffer = (char *)malloc(bufsize * sizeof(char));
  while ((getline(&buffer, &bufsize, stdin)) != EOF) {
    linep = 0;
    printf("Entered: %s\n", buffer);
    for (; (type = getop_by_line(s)) != '\n';) {
      handle_op(type, s);
    }
    handle_op(type, s);
  }
}

void handle_op(int type, char s[]) {
  double op2;
  switch (type) {
  case NUMBER:
    printf("NUMBER: %s\n", s);
    push(atof2(s));
    break;
  case NEGATIVE_NUMBER:
    printf("NEGATIVE_NUMBER: %s\n", s);
    push(atof2(s));
    break;
  case SIN:
    op2 = pop();
    printf("SIN(%f)\n", op2);
    push(sin(op2));
    break;
  case EXP:
    printf("EXP\n");
    push(exp(pop()));
    break;
  case POW:
    printf("POW\n");
    op2 = pop();
    push(pow(pop(), op2));
    break;
  case VARIABLE: {
    printf("VARIABLE: %c\n", s[0]);
    if (s[0] == '$') {
      push(vars[26]);
    } else {
      push(vars[s[0] - 97]);
    }

    break;
  }
  case '+':
    printf("PLUS\n");
    push(pop() + pop());
    break;
  case '*':
    printf("MULTIPLY\n");
    push(pop() * pop());
    break;
  case '-':
    printf("MINUS\n");
    op2 = pop();
    push(pop() - op2);
    break;
  case '/':
    printf("DIVIDE\n");
    op2 = pop();
    if (op2 != 0.0)
      push(pop() / op2);
    else
      printf("error: zero divisor\n");
    break;
  case '%':
    printf("MODULO\n");
    op2 = pop();
    if (op2 != 0.0)
      push((int)pop() % (int)op2);
    break;
  case '\n':
    printf("NEWLINE\n");
    double ans = pop();
    vars[26] = ans;
    printf("\t%.8g\n", ans);
    break;
  case ERROR:
    printf("error: something went wrong, presumably negative numbers\n");
  default:
    printf("error: unknown command%s\n", s);
    break;
  }
}

int calc() {
  int type;
  double op2;
  char s[MAXOP];

  while ((type = getop(s)) != EOF) {
    handle_op(type, s);
  }
  return 0;
}

/* push: push f onto value stack */
void push(double f) {
  if (sp < MAXVAL)
    val[sp++] = f;
  else
    printf("error: stack full, can't push %g\n", f);
}

/* pop: pop and return top value from stack */
double pop(void) {
  if (sp > 0)
    return val[--sp];
  else {
    printf("error: stack empty\n");
    return 0.0;
  }
}

/*  getop: get next operator or numeric operand */
int getop(char s[]) {
  int i, c;

  while ((s[0] = c = getch()) == ' ' || c == '\t') /* ignore whitespace */
    ;
  i = 0;
  if (c == '$') {
    return VARIABLE;
  }
  if (!isdigit(c) && c != '.' && !isalpha(c)) {
    if (c == '\n')
      return c;
    if (c == '-') {
      while (isdigit(s[++i] = c = getch())) /* check for negative number */
        ;
      if (isdigit(s[i - 1])) {
        s[i] = '\0';
        return NEGATIVE_NUMBER;
      } else {
        ungetch(c);
      }
    }

    s[1] = '\0';
    return s[0];           /* found operator */
  } else if (isalpha(c)) { /* check for math functions or variables */
    printf("LETTER(s)\n");
    if (!isalpha(s[++i] = c = getch())) { /* must be one letter variable */
      ungetch(c); /* put what we just consumed back on the stack */
      return VARIABLE;
    } else {
      while (isalpha(s[++i] = c = getch()))
        ;
      if (s[0] == 's' && s[1] == 'i' && s[2] == 'n') {
        ungetch(c);
        s[3] = '\0';
        return SIN;
      } else if (s[0] == 'e' && s[1] == 'x' && s[2] == 'p') {
        ungetch(c);
        s[3] = '\0';
        return EXP;
      } else if (s[0] == 'p' && s[1] == 'o' && s[2] == 'w') {
        ungetch(c);
        s[3] = '\0';
        return POW;
      }
    }
  }

  /*  Found operand */
  if (isdigit(c))
    while (isdigit(s[++i] = c = getch())) /*  get integer */
      ;
  if (c == '.')
    while (isdigit(s[++i] = c = getch())) /*  get fractional part */
      ;
  s[i] = '\0';
  if (c != EOF)
    ungetch(c);
  return NUMBER;
}

int getop_by_line(char s[]) {
  static int buf = EOF;
  int i, c;

  if (buf != EOF) {
    int tmp = buf;
    buf = EOF;
    return tmp;
  }

  while ((s[0] = c = getch_line()) == ' ' || c == '\t') /* ignore whitespace */
    ;

  i = 0;
  if (c == '$') {
    return VARIABLE;
  }
  if (!isdigit(c) && c != '.' && !isalpha(c)) {
    if (c == '\n')
      return c;
    if (c == '-') {
      while (isdigit(s[++i] = c = getch_line())) /* check for negative number */
        ;
      if (isdigit(s[i - 1])) {
        s[i] = '\0';
        return NEGATIVE_NUMBER;
      } else {
        buf = c;
      }
    }

    s[1] = '\0';
    return s[0];           /* found operator */
  } else if (isalpha(c)) { /* check for math functions or variables */
    printf("LETTER(s)\n");
    if (!isalpha(s[++i] = c = getch_line())) { /* must be one letter variable */
      return VARIABLE;
    } else {
      while (isalpha(s[++i] = c = getch_line()))
        ;
      if (s[0] == 's' && s[1] == 'i' && s[2] == 'n') {
        printf("SIN\n");
        buf = c;
        s[3] = '\0';
        return SIN;
      } else if (s[0] == 'e' && s[1] == 'x' && s[2] == 'p') {
        buf = c;
        s[3] = '\0';
        return EXP;
      } else if (s[0] == 'p' && s[1] == 'o' && s[2] == 'w') {
        buf = c;
        s[3] = '\0';
        return POW;
      }
    }
  }

  /*  Found operand */
  if (isdigit(c))
    while (isdigit(s[++i] = c = getch_line())) /*  get integer */
      ;
  if (c == '.')
    while (isdigit(s[++i] = c = getch_line())) /*  get fractional part */
      ;
  s[i] = '\0';

  return NUMBER;
}

int getch(void) /*  get a (possibly pushed back) character */
{
  return (bufp > 0) ? buf[--bufp] : getchar();
}

int getch_line(void) { return buffer[linep++]; }

void ungetch(int c) /*  push character back on input */
{
  if (bufp >= BUFSIZE)
    printf("ungetch: too many characters\n");
  else
    buf[bufp++] = c;
}

void print_stack() /* print the top of the stack */
{
  printf("peek: %f\n", val[sp]);
}

void duplicate_stack(double dup_stack[]) /* duplicate stack, assumes empty stack
                                            of size MAXVAL */
{
  int i;
  for (i = 0; i < sp; i++) {
    dup_stack[i] = val[i];
  }
}

#undef swap
int swap() /*  swap top two elements or return error code if not two elements
              available */
{
  if (sp < 1) {
    return -1;
  } else {
    double top = val[sp];
    val[sp] = val[sp - 1];
    val[sp - 1] = top;
    return 0;
  }
}

void clear() { /* Moving the index to 0 is "effectively" clearing the stack */
  bufp = 0;
}

void ungets(char s[]) { /* push string back on input */
  int i;
  for (i = 0; s[i] != '\0'; i++)
    ungetch(s[i]);
}

void itoa_recursive(int n, char s[]) {
  int digits = 1;
  int i = 0;

  int m = n;
  while (m / 10) {
    digits++;
    m = m / 10;
  }
  i = digits - 1;

  int sign;
  if ((sign = n) < 0) { /* record sign */
    s[i++] = '-';
    n = -n; /* make n positive */
  }
  if (n / 10)
    itoa_recursive(n / 10, s);
  s[i++] = (n % 10 + '0');
  s[i] = '\0';
}

#include <string.h>
void reverse_recursive(char s[], int i, int j) {
  int len = strlen(s);

  if (i != len - j && i < len - j) {
    char tmp = s[i];
    s[i] = s[len - j - 1];
    s[len - j - 1] = tmp;
    i++;
    j++;
    reverse_recursive(s, i, j);
  }
}
