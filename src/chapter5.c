#include <ctype.h>
#include <stdio.h>

int getch(void);
void ungetch(int);

/*
 * getint:  get next integer from input into *pn * As written, getint treats
 * a + or - not followed by a digit as a valid * representation of zero. Fix
 * it to push such a character back on the input.
 */
int getint(int *pn) {
  int c, sign, sawsign;

  while (isspace(c = getch())) /* skip white space */
    ;
  if (!isdigit(c) && c != EOF && c != '+' && c != '-') {
    ungetch(c); /* it's not a number */
    return 0;
  }
  sign = (c == '-') ? -1 : 1;
  if (sawsign = (c == '+' || c == '-'))
    c = getch();
  if (!isdigit(c)) {
    ungetch(c);
    if (sawsign)
      ungetch((sign == -1) ? '-' : '+');
    return 0;
  }
  for (*pn = 0; isdigit(c); c = getch())
    *pn = 10 * *pn + (c - '0');
  *pn *= sign;
  if (c != EOF)
    ungetch(c);
  return c;
}

int getfloat(float *pn) {
  int c, sign, sawsign;

  while (isspace(c = getch())) /* skip white space */
    ;
  if (!isdigit(c) && c != EOF && c != '+' && c != '-') {
    ungetch(c); /* it's not a number */
    return 1;
  }
  sign = (c == '-') ? -1 : 1;
  if (sawsign = (c == '+' || c == '-'))
    c = getch();
  if (!isdigit(c)) {
    ungetch(c);
    if (sawsign)
      ungetch((sign == -1) ? '-' : '+');
    return 0;
  }
  for (*pn = 0; isdigit(c); c = getch()) {
    *pn = 10 * *pn + (c - '0');
  }

  *pn *= sign;

  int div = 0;
  float frac;

  /* Found operand */
  if (c == '.') {
    if (isdigit(c = getch())) {
      for (frac = 0.0; isdigit(c); c = getch()) {
        frac = 10 * frac + (c - '0');
        div++;
      }
      for (; div > 0; div--) {
        frac = frac / 10.0;
      }
    }
  }

  *pn = *pn + frac;

  if (c != EOF)
    ungetch(c);

  return 0;
}

char *itoa_ptr(int *n, char *s) {
  int i = 0;

  int m = *n;
  while (m / 10) {
    i++;
    m = m / 10;
  }

  if (*n < 0) { /* record sign */
    s[i++] = '-';
    *n = -*n; /* make n positive */
  }
  if (*n / 10) {
    int tmp = *n / 10;
    itoa_ptr(&tmp, s);
  }
  s[i++] = (*n % 10 + '0');
  s[i] = '\0';
  return s;
}
