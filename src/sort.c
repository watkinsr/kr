#include "../include/debug.h"
#include <stdio.h>
#include <string.h>

// Exercise 5-14. Modify the sort program to handle a -r flag, which indicates
// sorting in reverse (decreasing) order. Be sure that -r works with -n.
//
// Exercise 5-15. Add the option -f to fold upper and lower case together, so
// that case distinctions are not made during sorting; for example, a and A
// compare equal.
//
// Exercise 5-16. Add the -d (``directory order'') option, which makes
// comparisons only on letters, numbers and blanks. Make sure it works in
// conjunction with -f.
//
// Exercise 5-17. Add a field-searching capability, so sorting may bee done on
// fields within lines, each field sorted according to an independent set of
// options. (The index for this book

#define MAXLINES 5000 /* max #lines to be sorted */
#define MAXLEN 1000
#define ALLOCSIZE 10000
char *lineptr[MAXLINES]; /* pointers to text lines */
char *alloc(int);

int readlines(char *lineptr[], int nlines);
void writelines(char *lineptr[], int nlines);
int my_getline(char *s, int lim);

void my_qsort(void *lineptr[], int left, int right, int (*comp)(void *, void *),
              int reverse);
int numcmp(char *, char *);
void swap(void *v[], int i, int j);

/* sort input lines */
int main(int argc, char *argv[]) {
  int nlines;      /* number of input lines read */
  int numeric = 0; /* 1 if numeric sort */
  int reverse = 0; /* 1 if reverse sort */
  int c;

  while (--argc > 0 && (*++argv)[0] == '-') {
    while (c = *++argv[0]) {
      switch (c) {
      case 'r':
        my_log("REVERSE");
        reverse = 1;
        break;
      case 'n':
        numeric = 1;
        my_log("NUMERIC");
        break;
      default:
        printf("sort: illegal option %c\n", c);
        argc = -1;
        return -1;
      }
    }
  }

  if ((nlines = readlines(lineptr, MAXLINES)) >= 0) {
    my_qsort((void **)lineptr, 0, nlines - 1,
             (int (*)(void *, void *))(numeric ? numcmp : strcmp), reverse);
    writelines(lineptr, nlines);
    return 0;
  } else {
    printf("input too big to sort\n");
    return 1;
  }
}

/* qsort:  sort v[left]...v[right] into increasing order */
void my_qsort(void *v[], int left, int right, int (*comp)(void *, void *),
              int reverse) {
  int i, last;

  void swap(void *v[], int, int);

  if (left >= right) /* do  nothing if array contains */
    return;          /* fewer than two elements */
  swap(v, left, (left + right) / 2);
  last = left;
  for (i = left + 1; i <= right; i++) {
    int comparison = (*comp)(v[i], v[left]);
    if (reverse ? comparison > 0 : comparison < 0) {
      swap(v, ++last, i);
    }
  }
  swap(v, left, last);
  my_qsort(v, left, last - 1, comp, reverse);
  my_qsort(v, last + 1, right, comp, reverse);
}

#include <stdlib.h>

/* numcmp:  compare s1 and s2 numerically */
int numcmp(char *s1, char *s2) {
  double v1, v2;

  v1 = atof(s1);
  v2 = atof(s2);
  if (v1 < v2)
    return -1;
  else if (v1 > v2)
    return 1;
  else
    return 0;
}

void swap(void *v[], int i, int j) {
  void *temp;

  temp = v[i];
  v[i] = v[j];
  v[j] = temp;
}

/* readlines:  read input lines */
int readlines(char *lineptr[], int maxlines) {
  int len, nlines;
  char *p, line[MAXLEN];

  nlines = 0;
  while ((len = my_getline(line, MAXLEN)) > 0)
    if (nlines >= maxlines || (p = alloc(len)) == NULL)
      return -1;
    else {
      line[len - 1] = '\0'; /* delete newline */
      strcpy(p, line);
      lineptr[nlines++] = p;
    }
  return nlines;
}

static char allocbuf[ALLOCSIZE]; /* storage for alloc */
static char *allocp = allocbuf;  /* next free position */

char *alloc(int n) /* return pointer to n characters */
{
  if (allocbuf + ALLOCSIZE - allocp >= n) { /* it fits */
    allocp += n;
    return allocp - n; /* old p */
  } else               /* not enough room */
    return 0;
}

/* my_getline:  get line into s, return length */
int my_getline(char *s, int lim) {
  int c, i;

  i = 0;
  while (--lim > 0 && (c = getchar()) != EOF && c != '\n') {
    i++;
    *s++ = c;
  }
  if (c == '\n') {
    i++;
    *s++ = c;
  }
  *s = '\0';
  return i;
}

/* writelines:  write output lines */
void writelines(char *lineptr[], int nlines) {
  int i;

  for (i = 0; i < nlines; i++)
    printf("%s\n", lineptr[i]);
}
