#include <stdio.h>
#include <limits.h>
#include "../include/chapter2.h"

/* squeeze: delete all c from s */
void squeeze(char s[], int c)
{
    int i, j;
    for (i = j = 0; s[i] != '\0'; i++)
        if (s[i] != c)
        s[j++] = s[i];
    s[j] = '\0';
}

/*  squeeze2: delete each character in s1 that matches any character in s2 */
void squeeze2(char s1[], char s2[])
{
    short charMap[26], i, j;
    // Initialize character map (assumes lower-case characters)
    for (i = 0; i < 26; i++)
        charMap[i] = -1;

    // Matches are defined by a value of 1
    for (i = 0; s2[i] != '\0'; i++) {
        charMap[s2[i] - 97] = 1;
    }

    for (i = j = 0; s1[i] != '\0'; i++) {
        short k = s1[i] - 97;
        if (charMap[k] == -1) {
            s1[j++] = s1[i];
        }
    }
    s1[j] = '\0';
}
/*  any: returns first location in s1 where any character of s2 occurs or -1 if none */
int any(char s1[], char s2[])
{
    short charMap[26], i, matchLocation = -1;
    // Initialize character map (assumes lower-case characters)
    for (i = 0; i < 26; i++)
        charMap[i] = -1;

    // Matches are defined by a value of 1
    for (i = 0; s2[i] != '\0'; i++) {
        charMap[s2[i] - 97] = 1;
    }

    for (i = 0; s1[i] != '\0' && matchLocation == -1; i++) {
        short k = s1[i] - 97;
        if (charMap[k] == 1)
            matchLocation = i;
    }
    return matchLocation;
}

/* getbits: get n bits from position p */
unsigned getbits(unsigned x, int p, int n)
{
    return (x >> (p+1-n)) & ~(~0 << n);
}

/* Exercise 2-6.
 * Write a function setbits(x,p,n,y) that returns x with the
 * n bits that begin at position p set to the rightmost n bits
 * of y, leaving the other bits unchanged.
 */
unsigned setbits(unsigned x, int p, int n, unsigned y)
{
    // get the right hand side, right of the bitfield
    unsigned rhs = x & ~(~0 << (p+1-n));
    /* printf("rhs = %i\n", rhs); */

    // get the left hand side: left of the bitfield
    unsigned lhs = ((x >> p+1) << p+1);
    /* printf("lhs = %i\n", lhs); */

    unsigned z = (getbits(y, n-1, n) << (p+1 - n)); // insert into bitfield position
    unsigned rhs_with_bits = z | rhs; // combine rhs with z
    return lhs | rhs_with_bits;
}
/*
** Exercise 2-7. Write a function invert(x,p,n) that returns x with the
** n bits that begin at position p inverted
** (i.e., 1 changed into 0 and vice versa), leaving the others unchanged.
*/
unsigned invert(unsigned x, int p, int n)
{
    unsigned inverted_bitfield = getbits(x,p,n) ^ ~(~0 << n);
    /* printf("inverted_bitfield = %i\n", inverted_bitfield); */
    /* printf("getbits(%i,%i,%i) = %i\n", x,p,n,getbits(x,p,n)); */

    return setbits(x,p,n,inverted_bitfield);

    /* return inverted_bitfield; */
}

/* unsigned rightrot(int x, int n) */
/* { */
/*     size_t s = sizeof(x) * CHAR_BIT; */
/*     unsigned z; */
/*     printf("getbits(%i,0,1) = %i\n", x, getbits(x,0,1)); */
/*     while (n > 0) */
/*     { */
/*         printf("x >> 1 = %i\n", x >> 1); */
/*         printf("%i - 1 = %i\n", s, (s - 1)); */
/*         z = setbits(x >> 1, s - 1, 1, getbits(x, 0, 1)); */
/*         printf("z = %i\n", z); */
/*         n--; */
/*     } */
/*     printf("amount of bits in x = %i\n", s); */
/*     return z; */
/* } */

unsigned int rightrot(unsigned int x, unsigned int n)
{
   /* calculate number of bits in type */
   size_t s = sizeof(x) * CHAR_BIT;
   size_t p;

   /* limit shift to range 0 - (s - 1) */
   if(n < s)
       p = n;
   else
       p = n % s;

   /* if either is zero then the original value is unchanged */
   if((0 == x) || (0 == p))
       return x;

   return (x >> p) | (x << (s - p));
}

/* bitcount: count 1 bits in x */
int bitcount(unsigned x)
{
    int b;

    for (b = 0; x != 0; x >>= 1)
        if (x & 01)
            b++;
    return b;
}
