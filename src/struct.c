#include "../include/debug.h"
#include "../include/chapter3.h"

char *int_to_str(int x, char s[]);
void log_int(int x);

int main() {
  struct point {
    int x;
    int y;
  };

  struct rect {
    struct point pt1;
    struct point pt2;

  };

  struct point p1 = { 1, 2 };
  struct point *pp = &p1;

  // printf(itoa(p1.x));
  int MAXSIZE=100;
  char s[MAXSIZE];
  dprint(p1.x);
  dprint(pp->x);
  dprint(p1.y);

  my_log("STRUCT");
  return 0;
}
