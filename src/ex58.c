#include "../include/debug.h"

int day_of_year(int year, int month, int day);
void month_day(int year, int yearday, int *pmonth, int *pday);

// int main(void) {
//   dprint(day_of_year(1994, 06, 06));
//   int m;
//   int d;
//   month_day(1994, 157, &m, &d);
//   dprint(m);
//   dprint(d);

//   /* Exercise 5-8: check errors */
//   month_day(2020, 367, &m, &d);
//   month_day(2019, 366, &m, &d);
//   month_day(2020, 0, &m, &d);
//   month_day(2019, -2, &m, &d);

//   dprint(day_of_year(2020, 02, 30));
//   dprint(day_of_year(2020, 02, -1));

//   return 0;
// }

static char daytab[2][13] = {
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

/* day_of_year:  set day of year from month & day */
int day_of_year(int year, int month, int day) {
  int i, leap;
  leap = year % 4 == 0 && year % 100 != 0 || year % 400 == 0;

  if (month <= 0 || month > 12 || day > 31 || day <= 0 ||
      (leap && day > 29 && month == 2) ||
      (!leap && day > 28 && month == 2)) { /* rudimentary error checking */
    return -1;
  }
  for (i = 1; i < month; i++)
    day += daytab[leap][i];
  return day;
}

/* month_day:  set month, day from day of year */
void month_day(int year, int yearday, int *pmonth, int *pday) {
  int i, leap;

  leap = year % 4 == 0 && year % 100 != 0 || year % 400 == 0;

  if (yearday <= 0 || (yearday > 366 && leap) || (yearday > 365 && !leap)) {
    my_log("Error: invalid parameters to month_day");
  }

  for (i = 1; yearday > daytab[leap][i]; i++)
    yearday -= daytab[leap][i];
  *pmonth = i;
  *pday = yearday;
}
