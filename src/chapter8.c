#include "../include/chapter8.h"
#include "../include/debug.h"

#include <sys/file.h>
// #include <sys/dir.h> /* local directory structure */
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#ifndef DIRSIZ
#define DIRSIZ 14
#endif

/* cat:  concatenate files, version 1 */
int my_cat(int argc, char *argv[]) {
  char buf[BUFSIZ];
  int f1, n;
  printf("arguments: %d\n", argc);
  my_log("my_cat");

  if (argc != 2)
      error("Usage: cat FILE");
  if ((f1 = open(argv[1], O_RDONLY, 0)) == -1)
      error("cat: can't open %s", argv[1]);

  while((n = read(f1, buf, BUFSIZ)) > 0)
      if (write(1, buf, n) != n)
          error("cat: write error on stdout");
  return 0;
}

/* error: print an error message and die */
void error(char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    fprintf(stderr, "error: ");
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);
    exit(1);
}

MY_FILE *my_fopen(char *name, char *mode)
{
    my_log("my_fopen");
    int fd;
    MY_FILE *fp;

    if (*mode != 'r' && *mode != 'w' && *mode != 'a')
        return NULL;
    for (fp = _my_iob; fp < _my_iob + OPEN_MAX; fp++)
        if (!fp->flags._READ && !fp->flags._WRITE)
            break;					/* found free slot */
    if (fp >= _my_iob + OPEN_MAX)	/* no free slots */
    {
        return NULL;
    }

    if (*mode == 'w')
        fd = creat(name, PERMS);
    else if (*mode == 'a') {
        if ((fd = open(name, O_WRONLY, 0)) == -1)
            fd = creat(name, PERMS);
        lseek(fd, 0L, 2);
    } else
        fd = open(name, O_RDONLY, 0);
    if (fd == -1)					/* couldn't access name */
        return NULL;
    fp->fd = fd;
    fp->cnt = 0;
    fp->base = NULL;
    if (*mode == 'r')
        fp->flags._READ = 1;
    else
        fp->flags._WRITE = 1;
    return fp;
}

int _fillbuf(MY_FILE *fp)
{
    my_log("_fillbuf");

    int bufsize;

    if (!fp->flags._READ)
        return EOF;
    bufsize = (fp->flags._UNBUF) ? 1 : BUFSIZ;
    if (fp->base == NULL)			/* no buffer yet */
        if ((fp->base = (char *) malloc(bufsize)) == NULL)
            return EOF;				/* can't get buffer */
    fp->ptr = fp->base;
    fp->cnt = read(fp->fd, fp->ptr, bufsize);
    if (--fp->cnt < 0) {
        if (fp->cnt == -1)
            fp->flags._EOF = 0;
        else
            fp->flags._ERR = 0;
        fp->cnt = 0;
        return EOF;
    }
    return (unsigned char) *fp->ptr++;
}
int _flushbuf(int x, MY_FILE *fp)
{
    my_log("_flushbuf");

    int bufsize;

    if (!fp->flags._WRITE)
        return EOF;
    bufsize = (fp->flags._UNBUF) ? 1 : BUFSIZ;
    if (fp->base == NULL)			/* no buffer yet */
    {
        if ((fp->base = (char *) malloc(bufsize)) == NULL)
            return EOF;				/* can't get buffer */
        fp->ptr = fp->base;
        fp->cnt = bufsize - 1;
        return *(fp)->ptr++ = x;
    }
    else
    {
        int n = fp->ptr - fp->base;
        write(fp->fd, fp->base, n);
        fp->ptr = fp->base;
        fp->cnt = bufsize - 1;
        return *(fp)->ptr++ = x;
    }
}

int my_fflush(MY_FILE *fp)
{
    if (!fp->flags._WRITE)
        return -1;
    _flushbuf(EOF, fp);
    if (fp->flags._ERR)
        return -1;
    return 0;
}

/* fclose */
int my_fclose(MY_FILE *fp)
{
    int fd;
    fd = fp->fd;
    my_fflush(fp);
    fp->cnt = 0;
    fp->ptr = NULL;
    if(fp->base != NULL)
        free(fp->base);
    struct _flags flags;
    fp->flags = flags;
    fp->fd = -1;
    return close(fd);
}

int my_fseek(MY_FILE *fp, long offset, int origin)
{
  my_log("fseek");
  if (!fp->flags._UNBUF && fp->base != NULL) {
    if (fp->flags._WRITE) {
      my_fflush(fp);
    } else if(fp->flags._READ) {
      fp->cnt = 0;
      fp->ptr = fp->base;
    }
  }
  return (lseek(fp->fd, offset, origin) < 0);
}

/* fsize: print the name of file "name" */
void fsize(char *name)
{
  // my_log("fsize");
  struct stat stbuf;

  if (stat(name, &stbuf) == -1) {
    fprintf(stderr, "fsize: can't access %s\n", name);
    return;
  }
  if ((stbuf.st_mode & S_IFMT) == S_IFDIR)
    dirwalk(name, fsize);
  printf("ino: %8ld, %8ld %s\n", stbuf.st_ino, stbuf.st_size, name);
}

#define MAX_PATH 1024

void dirwalk(char *dir, void (*fcn)(char *))
{
  // my_log("dirwalk");
  // printf("dirwalk(dir: %s)\n", dir);
  char name[MAX_PATH];
  struct dirent *dp;
  DIR *dfd;

  
  if ((dfd = opendir(dir)) == NULL) {
    fprintf(stderr, "dirwalk: can't open %s\n", dir);
    return;
  }
  while ((dp = readdir(dfd)) != NULL) {
    // printf("dp->name: %s\n", dp->d_name);
    if (strcmp(dp->d_name, ".") == 0 
    || strcmp(dp->d_name, "..") == 0) /* skip self and parent */ {
      // printf("skip self and parent\n");
      continue;
    }
    if (strlen(dir)+strlen(dp->d_name)+2 > sizeof(name))
    fprintf(stderr, "dirwalk: name %s %s too long \n",
      dir, dp->d_name);
    else {
      sprintf(name, "%s/%s", dir, dp->d_name);
      (*fcn)(name);
    }
  }
  closedir(dfd);
}
