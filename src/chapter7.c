#include "../include/chapter4.h"
#include "../include/chapter7.h"
#include "../include/debug.h"
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/* Exercise 7-3. Revise minprintf to handle more of the other facilities of
 * printf . */
// Exercise 7-5. Rewrite the postfix calculator of Chapter 4 to use scanf and/or
// sscanf to do the input and number conversion.

/*
 * Solution: To handle arbitrary length, use a scanf("%s", s) to get a word by
 * word, TOKENIZE that word and ude sscanf with that word to do input
 * conversion. Still maintain a stack etc. and do the operations and printing.
 */

/*
 * Exercise 7-6. Write a program to compare two files, printing the first line
 * where they differ.
 */

int strcmp(const char *, const char *);

void casing(int argc, char **argv) {
  int c;

  dprint(argc);
  dprints(argv[0]);

  if (strcmp(argv[0], "./lower") == 0) {
    while ((c = getchar()) != EOF)
      putchar(tolower(c));
  } else if (strcmp(argv[0], "./upper") == 0) {
    while ((c = getchar()) != EOF)
      putchar(toupper(c));
  }
}

void minprintf(
    char *fmt,
    ...) {    /* minprintf: minimal printf with variable argument list */
  va_list ap; /* points to each unnamed arg in turn */
  char *p, *sval;
  char word[MAXWORD];
  char *w = word;
  int ival;
  int left_adjustment = 0;
  double dval;
  va_start(ap, fmt); /* make ap point to 1st unnamed arg */
  for (p = fmt; *p; p++) {
    if (*p != '%') {
      putchar(*p);
      continue;
    }
    p = process_arg(p, ap);
    va_end(ap); /* clean up when done */
  }
}

char *process_arg(char *p, va_list ap) {
  char *sval;
  char word[MAXWORD];
  char *w = word;
  int ival;
  char str[MAXWORD];
  char *s = str;
  static int left_adjustment = 0;

  static int precision = DEFAULT_PRECISION; /* Default precision */

  double dval;

  switch (*++p) {
  case '.':
    // my_log("PRECISION");
    while (isdigit(*++p)) {
      *w++ = *p;
    }
    precision = atoi(word);
    // dprint(left_adjustment);
    p--;
    p = process_arg(p, ap);
    break;
  case '-':
    // my_log("MINUS");
    while (isdigit(*++p)) {
      *w++ = *p;
    }
    left_adjustment = atoi(word);
    // dprint(left_adjustment);
    p--;
    p = process_arg(p, ap);
    break;
  case 'd':
    // printf("[i]");
    ival = va_arg(ap, int);
    char str[MAXWORD];
    char *s = str;
    sprintf(str, "%d", ival);
    for (; *s && precision > 0; s++) {
      left_adjustment--;
      putchar(*s);
      precision--;
    }
    for (; left_adjustment > 0; --left_adjustment) {
      putchar(' ');
    }
    precision = DEFAULT_PRECISION;
    left_adjustment = 0;
    // printf("%d", ival);
    break;
  case 'f':
    // printf("[d]");
    dval = va_arg(ap, double);
    sprintf(str, "%lf", dval);
    for (; *s && precision > 0; s++) {
      left_adjustment--;
      putchar(*s);
      precision--;
    }
    for (; left_adjustment > 0; --left_adjustment) {
      putchar(' ');
    }
    precision = DEFAULT_PRECISION;
    left_adjustment = 0;
    // printf("%f", dval);
    break;
  case 's':
    // my_log("STRING ARGUMENT");
    for (sval = va_arg(ap, char *); *sval && precision > 0; sval++) {
      left_adjustment--;
      putchar(*sval);
      precision--;
    }
    for (; left_adjustment > 0; --left_adjustment) {
      putchar(' ');
    }
    precision = DEFAULT_PRECISION;
    left_adjustment = 0;
    break;
  default:
    putchar(*p);
    break;
  }
  return p;
}

void minscanf(char *fmt,
              ...) { /* minscanf: minimal scanf with variable argument list */
  my_log("minscanf");
  va_list ap; /* points to each unnamed arg in turn */
  char *p, *sval;
  char word[MAXWORD];
  char *w = word;
  int *ival;
  int c;
  double *dval;
  va_start(ap, fmt); /* make ap point to 1st unnamed arg */
  for (p = fmt; *p; p++) {
    if (*p != '%') {
      // putchar(*p);
      continue;
    }
    switch (*++p) {
    case 'd':
      my_log("integer argument");
      ival = va_arg(ap, int *);

      while (isspace(c = getch()))
        ;

      *w++ = c;
      while (isdigit(c = getch())) {
        *w++ = c;
      }
      *w = '\0';
      *ival = atoi(word);
      w = word;
      break;
    case 'e':
    case 'f':
    case 'g':
      my_log("double argument");
      dval = va_arg(ap, double *);
      while (isspace(c = getch()))
        ;

      *w++ = c;
      while (!isspace(c = getch())) {
        *w++ = c;
      }
      *w = '\0';
      *dval = atof2(word);
      w = word;
      break;
    case 's':
      my_log("string argument");
      while (isspace(c = getch()))
        ;

      *w++ = c;
      while (isalpha(c = getch())) {
        *w++ = c;
      }
      *w = '\0';
      sval = va_arg(ap, char *);
      strcpy(sval, word);
      w = word;
      break;
    default:
      printf("LOG: Couldn't process this formatting: %%%c\n", *p);
      break;
    }
  }
  va_end(ap); /* clean up when done */
}

/* cat:  concatenate files, version 1 */
int cat(int argc, char *argv[]) {
  my_log("cat");
  FILE *fp;
  void filecopy(FILE *, FILE *);

  if (argc == 1) /* no args; copy standard input */
    filecopy(stdin, stdout);
  else
    while (--argc > 0)
      if ((fp = fopen(*++argv, "r")) == NULL) {
        printf("cat: can't open %s\n", *argv);
        return 1;
      } else {
        filecopy(fp, stdout);
        fclose(fp);
      }
  return 0;
}

/* filecopy:  copy file ifp to file ofp */
void filecopy(FILE *ifp, FILE *ofp) {
  int c;

  while ((c = getc(ifp)) != EOF)
    putc(c, ofp);
}

int diff(char *s1, char *s2) {
  my_log("DIFF");
  char line1[MAXLINE];
  char line2[MAXLINE];


  FILE *f1 = fopen(s1, "r");
  if (f1 == NULL) {
    printf("diff: can't open %s\n", s1);
  }
  FILE *f2 = fopen(s2, "r");
  if (f2 == NULL) {
    printf("diff: can't open %s\n", s2);
  }

  for (; fgets(line1, MAXLINE, f1) != NULL &&
         fgets(line2, MAXLINE, f2) != NULL;) {
    if (strcmp(line1, line2) != 0) {
      printf("First differ: \n%s -> %s%s -> %s", s1, line1, s2, line2);
      break;
    }
  }

  return 0;
}
