#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "../include/chapter3.h"

/* Converts characters like \n \t to actual escape sequences
 * Copy t to s
 * */
void escape(char s[], char t[])
{
    char c;
    int i, j;
    for (i = j = 0; (c = t[i]) != '\0'; i++) {
        switch (c) {
            case '\t':
                s[j] = '\\';
                s[j+1] = 't';
                j+=2;
                break;
            case '\n':
                s[j] = '\\';
                s[j+1] = 'n';
                j+=2;
                break;
            default:
                s[j] = c;
                j+=1;
                break;
        }
    }
    s[j] = '\0';
}
/* Exercise 3-3. Write a function expand(s1,s2) that expands shorthand notations like a-z in */
/* the string s1 into the equivalent complete list abc...xyz in s2 . Allow for letters of either */
/* case and digits, and be prepared to handle cases like a-b-c and a-z0-9 and -a-z . Arrange */
/* that a leading or trailing - is taken literally. */

int expand(char s1[], char s2[])
{
    printf("expand(%s, s2)\n", s1);
    int i = 0;
    int j = 0;
    int k;

    // Check for leading -
    if (s1[i] == '-') {
        s2[j++] = '-';
        i++;
    }

    while(s1[i] != '\0') {
        switch (s1[i]) {
            case '-': {
                char begin = s1[i - 1];
                char terminal = s1[i + 1];
                for(k = 0; (begin + k) <= (terminal - 0); k++) {
                    char c = begin + k;
                    if (j > 0 && s2[j - 1] == c) {}
                    else {
                        s2[j] = c;
                        j++;
                    }
                }
                i+=2;
                break;
            }
            default:
                i++;
        }
    }
    s2[j] = '\0';
    return 0;
}

/* itoa: convert n to characters in s */
void itoa(int n, char s[])
{
    int overflow = 0;
    int i, sign;
    if ((sign = n) < 0) {        /* record sign */
        if (n == INT_MIN) {
            n = -(n + 1);
            overflow = 1;
        } else {
            n = -n;                /* make n positive */
        }
    }
    i = 0;
    do {                       /* generate digits in reverse order */
        s[i++] = n % 10 + '0'; /* get next digit */
    } while ((n /= 10) > 0);   /* delete it */
    if (sign < 0) {
        if (overflow)
            s[0] = s[0] + 1;
        s[i++] = '-';
    }
    s[i] = '\0';
    reverse(s);
}

void itob(int n, char s[], int b)
{
    int is_hex = 0;

    if (b == 16)
        is_hex = 1;

    int overflow = 0;
    int i, sign;
    if ((sign = n) < 0) {        /* record sign */
        if (n == INT_MIN) {
            n = -(n + 1);
            overflow = 1;
        } else {
            n = -n;                /* make n positive */
        }
    }
    i = 0;
    do {                       /* generate digits in reverse order */
        if (is_hex && n % b > 9) {
            switch (n % b) {
                case 10:
                    s[i++] = 'A';
                    break;
                case 11:
                    s[i++] = 'B';
                    break;
                case 12:
                    s[i++] = 'C';
                    break;
                case 13:
                    s[i++] = 'D';
                    break;
                case 14:
                    s[i++] = 'E';
                    break;
                case 15:
                    s[i++] = 'F';
                    break;
            }
        } else {
            s[i++] = n % b + '0'; /* get next digit */
        }
    } while ((n /= b) > 0);   /* delete it */
    if (sign < 0) {
        if (overflow)
            s[0] = s[0] + 1;
        s[i++] = '-';
    }
    s[i] = '\0';
    reverse(s);
}

/* reverse: reverse string s in place */
void reverse(char s[])
{
    int c, i, j;
    for (i = 0, j = strlen(s)-1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* Exercise 3-6. Write a version of itoa that accepts three arguments instead of two. The third */
/* argument is a minimum field width; the converted number must be padded with blanks on the */
/* left if necessary to make it wide enough. */

/* itoa: convert n to characters in s */
void itoa2(int n, char s[], int field_width)
{
    int overflow = 0;
    int i, sign;
    if ((sign = n) < 0) {        /* record sign */
        if (n == INT_MIN) {
            n = -(n + 1);
            overflow = 1;
        } else {
            n = -n;                /* make n positive */
        }
    }
    i = 0;
    do {                       /* generate digits in reverse order */
        s[i++] = n % 10 + '0'; /* get next digit */
    } while ((n /= 10) > 0);   /* delete it */
    if (sign < 0) {
        if (overflow)
            s[0] = s[0] + 1;
        s[i++] = '-';
    }
    for (; i < field_width; i++) {
        s[i] = ' ';
    }
    s[i] = '\0';
    reverse(s);
}

