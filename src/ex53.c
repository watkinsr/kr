/* ex 5-3, p107 */

#include <stdio.h>
void strcpy(char *s, char *t)
{
    while (*s++ = *t++);
}

void strcat(char *s, char *t)
{
    while(*s)
    {
        ++s;
    }
    strcpy(s,t);
}

// int main ()
// {
//     char buff[128];
//     strcpy(buff, "Hello");
//     char *t = "World";
//     printf("Buff after cpy: %s\n", buff);
//     strcat(buff, t);
//     printf("Buff after cat: %s\n", buff);
// }

