#include <stdio.h>
#include "../include/debug.h"

int strlen(char *s)
{
    int i = 0;
    while(*s++ != '\0')
        i++;
    return i;

}
int strcmp(char *s, char *t)
{
    for ( ; *s == *t; s++, t++)
        if (*s == '\0')
            return 0;
        return *s - *t;
}

int strend(char *s, char *t)
{
    my_log("strend");
    int i;
    int s_size = strlen(s);
    int t_size = strlen(t);
    if (t_size < s_size)
    {
     	for (i = 0; i < t_size; i++)
     	{
		s++;
     	}
     	if(strcmp(s, t) == 0)
         	return 1;
     	else
         	return 0;
    }
    return 0;
}

// int main ()
// {
//     my_log("Exercise 5-4");
//     dprint(strend("HelloWorld", "World"));
//     dprint(strend("HelloWorld", "HeyFrens"));
//     return 0;
// }
