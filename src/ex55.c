#include "../include/debug.h"
#include <stdio.h>

#define MAX 20

char *my_strncpy(char *s, const char *t, int n);
char *my_strncat(char *s, char *t, int n);
int my_strncmp(const char *s, const char *t, int n);

// int main() {
//   my_log("Exercise 5-5");

//   char s2[MAX];
//   my_strncpy(s2, "Hello, World!", 5);
//   dprints(s2);

//   char s3[] = "Well, well! ";
//   my_strncat(s3, "Hello, World!", 5);
//   dprints(s3);

//   dprint(my_strncmp("Hello, there", "Hello fren", 5));
//   dprint(my_strncmp("Hello, there", "Hello fren", 7));

//   return 0;
// }

char *my_strncpy(char *s, const char *t, int n) {
  int i;

  for (i = 0; i < n; i++)
    *(s + i) = *(t + i);

  *(s + i) = '\0';

  return s;
}

/* copies at most n characters of t to end of s */
char *my_strncat(char *s, char *t, int n) {
  int i = 0;
  while (*++s)
    ;
  for (i = 0; i < n; i++)
    *(s + i) = *(t + i);
  *(s + i) = '\0';

  return s;
}

/* compares at most n characters of t to s */
int my_strncmp(const char *s, const char *t, int n) {
  int i;

  for (i = 0; s[i] == t[i]; i++) {
    if (n - (i + 1) == 0)
      return 0;
  }
  return s[i] - t[i];
}
