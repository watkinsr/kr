#include "../include/chapter6.h"
#include "../include/chapter5.h"
#include "../include/debug.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Exercise 6-1. Our version of getword does not properly handle underscores,
// string constants, comments, or preprocessor control lines. Write a better
// version.

// Exercise 6-2. Write a program that reads a C program and prints in
// alphabetical order each group of variable names that are identical in the
// first 6 characters, but different somewhere thereafter. Don't count words
// within strings and comments. Make 6 a parameter that can be set from the
// command line.

// Exercise 6-3. Write a cross-referencer that prints a list of all words in a
// document, and for each word, a list of the line numbers on which it occurs.
// Remove noise words like ``the,''
// ``and,'' and so on.

/*
 *  We don't wanna ignore comments etc here BUT we do wanna ignore punctuation /
 * underscores If we want to make use of the getword function, it would need to
 * be configurable. So one way would be to set default values for say, ignore X
 */

// Exercise 6-4. Write a program that prints the distinct words in its input
// sorted into decreasing order of frequency of occurrence. Precede each word by
// its count.

/*
 *  Solution:
 *
 *  Form a tree of count and word.
 *
 *  Walk down the entire tree, gathering an array of TREESIZE of
 *  pairs: where the pair is: count, word
 *
 *  Then, form a new binary search tree but this time, sorted by count,
 *  rather than word.
 *
 *  Finally, print that binary search tree.
 *
 */

// Exercise 6-5. Write a function undef that will remove a name and definition
// from the table maintained by lookup and install .

// Exercise 6-6. Implement a simple version of the #define processor (i.e., no
// arguments) suitable for use with C programs, based on the routines of this
// section. You may also find getch and ungetch helpful.

struct key keytab[] = {"auto", 0, "break", 0, "case", 0, "char", 0, "const", 0,
                       "continue", 0, "default", 0,
                       /* ... */
                       "unsigned", 0, "void", 0, "volatile", 0, "while", 0};

struct tlist *tl;

int COMPARE_SIZE = 6;
static char buf[BUFSIZE]; /*  buffer for ungetch */
static int bufp = 0;      /*  next free position in buf */

/* Configurable getword */
int DISABLE_PREPROCESSOR = 1;
int DISABLE_COMMENTS = 1;
int DISABLE_STRING_CONSTANT = 1;

/* Configurable addtree */
int ENABLE_GROUPS = 1;
int ENABLE_LINENO_TRACKING = 0;

int lineno = 1;

/* count C keywords */
int count_keywords() {
  int n;
  char word[MAXWORD];

  while (getword(word, MAXWORD, NULL) != EOF)
    if (isalpha(word[0]))
      if ((n = my_binsearch(word, keytab, NKEYS)) >= 0)
        keytab[n].count++;
  for (n = 0; n < NKEYS; n++)
    if (keytab[n].count > 0)
      printf("%4d %s\n", keytab[n].count, keytab[n].word);
  return 0;
}

int count_grouped_variable_names(int argc, char *argv[]) {
  // dprint(argc);
  if (argc > 1) {
    COMPARE_SIZE = atoi(argv[argc - 1]);
  }
  FILE *fp = fopen(FILENAME, "r");
  dprint(COMPARE_SIZE);
  my_log("Count grouped variable names");
  struct tnode *root;
  char word[MAXWORD];

  root = NULL;
  while (getword(word, MAXWORD, fp) != EOF) {
    // dprints(word);
    if (isalpha(word[0])) {
      // my_log("is alpha");
      root = addtree(root, word);
      // my_log("after addtree");
    }
  }
  my_log("count_grouped_variable_names: try to print tree");
  treeprint(root);
  return 0;
}

int cross_referencer(int argc, char *argv[]) {
  // dprint(argc);
  ENABLE_LINENO_TRACKING = 1;
  ENABLE_GROUPS = 0;
  my_log("cross_referencer");
  if (argc > 1) {
    COMPARE_SIZE = atoi(argv[argc - 1]);
  }

  FILE *fp = fopen(FILENAME, "r");

  dprint(COMPARE_SIZE);
  struct tnode *root;
  char word[MAXWORD];

  root = NULL;

  DISABLE_COMMENTS = DISABLE_PREPROCESSOR = DISABLE_STRING_CONSTANT = 0;
  printf("%d: ", lineno);
  while (getword(word, MAXWORD, fp) != EOF) {
    // dprints(word);
    if (isalpha(word[0])) {
      // my_log("is alpha");
      root = addtree(root, word);
      // my_log("after addtree");
    }
  }
  // my_log("cross_referencer: try to print tree");
  treeprint(root);
  return 0;
}

int distinct_words(int argc, char *argv[]) {
  ENABLE_LINENO_TRACKING = 0;
  ENABLE_GROUPS = 0;
  DISABLE_COMMENTS = DISABLE_PREPROCESSOR = DISABLE_STRING_CONSTANT = 0;

  my_log("distinct_words");
  if (argc > 1) {
    COMPARE_SIZE = atoi(argv[argc - 1]);
  }

  FILE *fp = fopen(FILENAME, "r");

  dprint(COMPARE_SIZE);
  struct tnode *root;
  char word[MAXWORD];

  root = NULL;

  printf("%d: ", lineno);
  while (getword(word, MAXWORD, fp) != EOF) {
    // dprints(word);
    if (isalpha(word[0])) {
      // my_log("is alpha");
      root = addtree(root, word);
      // my_log("after addtree");
    }
  }

  // treeprint(root);

  struct tlist *tl = tlalloc();
  gather_tree(root, tl);
  struct tlist *tl_copy = tl;
  int len = 0;
  while (tl_copy != NULL) {
    len++;
    tl_copy = tl_copy->next;
  };

  dprint(len);

  struct pair xs[len];

  tl_copy = tl;
  int i = 0;

  while (tl_copy != NULL) {
    xs[i].count = tl_copy->count;
    xs[i].word = tl_copy->word;
    i++;
    tl_copy = tl_copy->next;
  }

  qsort(xs, len, sizeof(struct pair), compare);

  for (i = 0; i < len; i++) {
    printf("%d %s\n", xs[i].count, xs[i].word);
  }

  return 0;
}

int ENABLE_FILE_PRINTING = 0; /* Flag for file printing */

int compare(const void *a, const void *b) {
  struct pair pa = *((struct pair *)a);
  struct pair pb = *((struct pair *)b);
  if (pa.count == pb.count)
    return 0;
  else if (pa.count > pb.count)
    return -1;
  else
    return 1;
}

/* binsearch:  find word in tab[0]...tab[n-1] */
int my_binsearch(char *word, struct key tab[], int n) {
  int cond;
  int low, high, mid;

  low = 0;
  high = n - 1;
  while (low <= high) {
    mid = (low + high) / 2;
    if ((cond = strcmp(word, tab[mid].word)) < 0)
      high = mid - 1;
    else if (cond > 0)
      low = mid + 1;
    else
      return mid;
  }
  return -1;
}

/* getword:  get next word or character from input */
int getword(char *word, int lim, FILE *fp) {
  int c, cc, getch(void);
  void ungetch(int);
  char *w = word;

  while (isspace(c = my_getch(fp))) {
    printf("%c", c);
    if (c == '\n') {
      cc = my_getch(fp);
      my_ungetch(cc);
      if (cc == EOF)
        return EOF;
      else
        printf("%d: ", ++lineno);
    }
  }

  if (c != EOF) {
    *w++ = c;
    printf("%c", c);
  } else {
    printf("<<EOF>>");
    return EOF;
  }

  if (!isalpha(c)) {
    if (c == '#' && DISABLE_PREPROCESSOR) {
      my_log("Found preprecessor statement");
      handle_preprocessor(w, fp);
      return '\0';
    } else if (c == '/' && DISABLE_COMMENTS) {
      if ((c = my_getch(fp)) == '/' || c == '*') {
        my_log("Found comment");
        handle_comment(w, c, fp);
        return '\0';
      } else {
        if (c == '\n') {
          lineno++;
          printf("%d: ", lineno);
        }
        // my_ungetch(c);
        *w = '\0';
        return c;
      }
    } else if (c == '"' && DISABLE_STRING_CONSTANT) {
      my_log("Found string constant");
      handle_stringconstant(w, fp);
      return '\0';
    } else {
      *w = '\0';
      // dprint(c);
      return c;
    }
  }
  for (; --lim > 0; w++) {
    *w = my_getch(fp);
    printf("%c", *w);
    if (*w == '\n') {
      lineno++;
      printf("%d: ", lineno);
    }
    if (!isalpha(*w)) {
      // printf("(%c)", *w);
      // my_ungetch(*w);
      // w++;
      break;
    }
  }
  *w = '\0';
  // printf(" (%s)", word);
  // dprints(word);
  return word[0];
}

int define_processor() {
  my_log("define_processor");

  FILE *fp = fopen("define.txt", "r");

  char word[MAXWORD];
  int type;

  if (ENABLE_LINENO_TRACKING)
    printf("%d: ", lineno);

  while ((type = getterm(word, MAXWORD, fp)) != EOF) {
    switch (type) {
    case REPLACEMENT:
      my_log("REPLACEMENT");
      break;
    case OTHER:
      // my_log("OTHER");
      break;
    default:
      // my_log("default");
      break;
    }
  }

  return 0;
}

int DEFINE = 0;

/* getterm:  get next term from input */
int getterm(char *word, int lim, FILE *fp) {
  int c, cc, getch(void);
  void ungetch(int);
  char *w = word;

  /* Handle whitespace */
  while (isspace(c = my_getch(fp))) {
    if (ENABLE_FILE_PRINTING)
      printf("%c", c);
    if (c == '\n') {
      cc = my_getch(fp);
      my_ungetch(cc);
      if (cc == EOF)
        return EOF;
      else {
        if (ENABLE_LINENO_TRACKING)
          printf("%d: ", ++lineno);
      }
    }
  }

  if (c != EOF) {
    *w++ = c;
    if (ENABLE_FILE_PRINTING)
      printf("%c", c);
  } else {
    printf("<<EOF>>");
    return EOF;
  }

  if (!isalpha(c)) {
    if (c == '#') { /* define statement */
      while (isalpha(c = my_getch(fp))) {
        if (ENABLE_FILE_PRINTING)
          printf("%c", c);
        *w++ = c;
      }
      if (strcmp(word, "#define") == 0) { /* Handle define statement */
        char name[lim];
        char defn[lim];
        char *n = name;
        char *d = defn;
        while (isspace(c = my_getch(fp)))
          ;

        for (*n++ = c; --lim > 0 && isalpha(c = my_getch(fp)); *n++ = c)
          ;
        *n = '\0';
        dprints(name);

        while (isspace(c = my_getch(fp)))
          ;

        for (*d++ = c; --lim > 0 && !isspace(c = my_getch(fp)); *d++ = c)
          ;
        *d = '\0';
        dprints(defn);

        install(name, defn);

        dprints(lookup(name)->name);
        return REPLACEMENT;
      }
      my_ungetch(c);
      *w = '\0';
      return word[0];
    } else {
      if (c == '\n') {
        lineno++;
        if (ENABLE_LINENO_TRACKING)
          printf("%d: ", lineno);
      }
      // my_ungetch(c);
      *w = '\0';
      return c;
    }
  }

  for (; --lim > 0 && isalpha(*w++ = my_getch(fp));)
    ;
  *(--w) = '\0';
  struct nlist *np;
  if ((np = lookup(word))) {
	  word = np->defn;
	  my_log("Word found in lookup table");
  }
  dprints(word);
  return word[0];
}

void handle_preprocessor(char *w, FILE *fp) {
  // my_log("Handle preprocessor");
  int c;
  while ((c = my_getch(fp)) != '\n')
    ;
  // *w++ = c;
  // my_log("test");
  // my_ungetch(c); // Put newline back on the stack
  lineno++;
  printf("%d: ", lineno);
}

void handle_comment(char *w, int c, FILE *fp) {
  if (c == '*') {
    // my_log("Multi-line comment");
    // my_log("Handle comment");
    while (1) {
      if ((c = my_getch(fp)) == '*') {
        if ((c = my_getch(fp)) == '/')
          break;
      } else if (c == '\n') {
        lineno++;
        printf("%d: ", lineno);
      }
    }
    // my_log("Multi-line comment finished");
  } else {
    // my_log("one-line comment");
    // my_log("Handle comment");
    while ((c = my_getch(fp)) != '\n')
      ;
    // *w++ = c;
    // my_ungetch(c); // Put newline back on the stack
  }
}

void handle_stringconstant(char *w, FILE *fp) {
  // my_log("Handle string constant");
  int c;
  while ((c = my_getch(fp)) != '"')
    ;
  // *w++ = c;
  // my_ungetch(c); // Put newline back on the stack
}

/* addtree:  add a node with w, at or below p */
struct tnode *addtree(struct tnode *p, char *w) {
  int cond;

  if (p == NULL) { /* a new word has arrived */
    // my_log("New word");
    p = talloc(); /* make a new node */
    p->word = strdup(w);
    p->count = 1;
    p->left = p->right = NULL;
    if (ENABLE_GROUPS) {
      p->group = galloc();
      p->group->variables = NULL;
    }
    if (ENABLE_LINENO_TRACKING) {
      p->lhead = lalloc();
      p->lhead->curr = itoa_ptr(&lineno, (char *)malloc(sizeof(char)));
      p->lhead->next = lalloc();
      p->llast = p->lhead->next;
    }
  } else if ((cond = ENABLE_GROUPS ? strncmp(w, p->word, COMPARE_SIZE)
                                   : strcmp(w, p->word)) == 0) {
    // my_log("COMPARE SUCCESS");
    p->count++; /* repeated word */
    if (ENABLE_GROUPS) {
      p->group->variables = add_group_tree(p->group->variables, w);
    }
    if (ENABLE_LINENO_TRACKING) {
      p->llast->curr = itoa_ptr(&lineno, (char *)malloc(sizeof(char)));
      p->llast->next = lalloc();
      p->llast->next->curr = "-1"; // Useful for printing later
      p->llast = p->llast->next;
    }
  } else if (cond < 0) /* less than into left subtree */
    p->left = addtree(p->left, w);
  else /* greater than into right subtree */
    p->right = addtree(p->right, w);
  return p;
};

/* addtree:  add a node with w, at or below p */
struct tnode *add_group_tree(struct tnode *p, char *w) {
  // my_log("Add group tree");
  int cond;
  if (p == NULL) { /* a new word has arrived */
    // my_log("Grouptree: new word");
    p = talloc(); /* make a new node */
    p->word = strdup(w);
    p->count = 1;
    p->left = p->right = NULL;
    p->group = NULL;
  } else if ((cond = strcmp(w, p->word)) == 0) {
    // my_log("Grouptree: same word");
    p->count++;        /* repeated word */
  } else if (cond < 0) /* less than into left subtree */
    p->left = addtree(p->left, w);
  else /* greater than into right subtree */
    p->right = addtree(p->right, w);
  return p;
};

/* treeprint:  in-order print of tree p */
void treeprint(struct tnode *p) {

  char s[1024];

  if (ENABLE_GROUPS) {
    if (p != NULL) {
      char *groupstr = (p->group != NULL) ? "GROUP:" : "";
      treeprint(p->left);
      printf("%s %4d %s\n", groupstr, p->count, p->word);
      if (p->group != NULL) {
        // my_log("treeprint: try to print group variables");
        treeprint(p->group->variables);
      }
      treeprint(p->right);
    }
  } else if (ENABLE_LINENO_TRACKING) {
    if (p != NULL) {
      treeprint(p->left);
      strcpy(s, "[");
      get_lineno_list(p->lhead, s);
      strcat(s, "]");
      printf("%4d %s %s\n", p->count, p->word, s);
      // dprints(s);
      treeprint(p->right);
    }
  } else {
    if (p != NULL) {
      treeprint(p->left);
      printf("%4d %s\n", p->count, p->word);
      treeprint(p->right);
    }
  }
}

void *gather_tree(struct tnode *node, struct tlist *last) {
  if (node == NULL)
    return last;

  last->count = node->count;
  last->word = node->word;

  if (node->left != NULL) {
    last->next = tlalloc();
    last = gather_tree(node->left, last->next);
  }
  if (node->right != NULL) {
    last->next = tlalloc();
    last = gather_tree(node->right, last->next);
  }

  return last;
}

void *get_lineno_list(struct lineno *x, char s[]) {
  strcat(s, x->curr);
  if (x->next->curr != "-1" && x->next->curr != NULL) {
    strcat(s, ",");
    get_lineno_list(x->next, s);
  }
}

char *my_strdup(char *s) /* make a duplicate of s */
{
  char *p;

  p = (char *)malloc(strlen(s) + 1); /* +1 for '\0' */
  if (p != NULL)
    strcpy(p, s);
  return p;
}

int my_getch(FILE *fp) /*  get a (possibly pushed back) character */
{
  // my_log("my_getch");
  int c;
  if (fp != NULL) {
    c = (bufp > 0) ? buf[--bufp] : fgetc(fp);
    // printf("%c", c);
  } else {
    c = (bufp > 0) ? buf[--bufp] : getchar();
    // printf("%c", c);
  }
  return c;
}

void my_ungetch(int c) /*  push character back on input */
{
  // my_log("(my_ungetch)");
  // dprint(bufp);
  if (bufp >= BUFSIZE)
    printf("ungetch: too many characters\n");
  else {
    buf[bufp++] = c;
    // dprint(bufp);
  }
}

/* talloc:  make a tnode */
struct tnode *talloc(void) {
  return (struct tnode *)malloc(sizeof(struct tnode));
};

struct group *galloc(void) {
  return (struct group *)malloc(sizeof(struct group));
}
struct lineno *lalloc(void) {
  return (struct lineno *)malloc(sizeof(struct lineno));
}
struct tlist *tlalloc(void) {
  return (struct tlist *)malloc(sizeof(struct tlist));
}

/* hash: form hash value for string s */
unsigned hash(char *s) {
  unsigned hashval;
  for (hashval = 0; *s != '\0'; s++)
    hashval = *s + 31 * hashval;

  return hashval % HASHSIZE;
}

/* lookup: look for s in hashtab */
struct nlist *lookup(char *s) {
  struct nlist *np;

  for (np = hashtab[hash(s)]; np != NULL; np = np->next)
    if (strcmp(s, np->name) == 0)
      return np; /* found */
  return NULL;   /* not found */
}

/* install: put (name, defn) in hashtab */
struct nlist *install(char *name, char *defn) {
  struct nlist *np;
  unsigned hashval;
  if ((np = lookup(name)) == NULL) { /* not found */
    np = (struct nlist *)malloc(sizeof(*np));
    if (np == NULL || (np->name = strdup(name)) == NULL)
      return NULL;
    hashval = hash(name);
    np->next = hashtab[hashval];
    hashtab[hashval] = np;
  } else                    /* already there */
    free((void *)np->defn); /*free previous defn */
  if ((np->defn = strdup(defn)) == NULL)
    return NULL;
  return np;
}

void undef(char *s) {
  int found = 0;

  unsigned hashval;

  struct nlist *np;
  struct nlist *prev = NULL;

  hashval = hash(s);

  np = hashtab[hashval];

  while (!found && np != NULL) {
    if (strcmp(s, np->name) == 0) {
      found = 1;
      if (prev == NULL) {
        hashtab[hashval] = NULL;
      } else {
        prev->next = np->next;
      }
    } else {
      prev = np;
      np = np->next;
    }
  }
}

void print_nlist(int i) {
  my_log("print_nlist");
  struct nlist *np = hashtab[i];

  while (np != NULL) {
    dprints(np->name);
    dprints(np->defn);
    np = np->next;
  }
}
