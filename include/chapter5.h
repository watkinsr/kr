#ifndef __CHAPTER5_H_
#define __CHAPTER5_H_

int getint(int *pn);
int getfloat(float *pn);
char *itoa_ptr(int *n, char *s);

#endif //
