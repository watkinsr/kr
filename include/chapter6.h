#ifndef __CHAPTER6_H_
#define __CHAPTER6_H_

#define MAXWORD 100
#define MAXGROUP 1000
#define NKEYS (sizeof keytab / sizeof(keytab[0]))
#define BUFSIZE 100
#define HASHSIZE 101

#define REPLACEMENT 1
#define OTHER 2

#define FILENAME "chapter6.c"

#include <stdio.h>

static struct nlist *hashtab[HASHSIZE];

struct key {
  char *word;
  int count;
};

struct tnode {          /* the tree node: */
  char *word;           /* points to the text */
  int count;            /* number of occurrences */
  struct tnode *left;   /* left child */
  struct tnode *right;  /* right child */
  struct group *group;  /* Group of related variables */
  struct lineno *lhead; /* Track line numbers in a linked list */
  struct lineno *llast; /* Track line numbers in a linked list */
};

struct lineno {
  char *curr;
  struct lineno *next;
};

struct group {
  struct tnode *variables;
};

struct tlist {
  char *word;
  int count;
  struct tlist *next;
};

struct pair {
  int count;
  char *word;
};

struct nlist {        /* table entry: */
  struct nlist *next; /* next entry in chain */
  char *name;         /* defined name */
  char *defn;         /* replacement text */
};

int getword(char *, int, FILE *);
int my_binsearch(char *, struct key *, int);
int count_keywords();
void handle_preprocessor(char *, FILE *);
void handle_comment(char *, int, FILE *);
void handle_stringconstant(char *, FILE *);

struct tnode *addtree(struct tnode *, char *);
struct tnode *add_group_tree(struct tnode *, char *);
void treeprint(struct tnode *);
char *my_strdup(char *);

struct tnode *talloc(void);
struct group *galloc(void);
struct lineno *lalloc(void);
struct tlist *tlalloc(void);

int count_grouped_variable_names(int, char *[]);
int cross_referencer(int, char *[]);
int distinct_words(int, char *[]);

int my_getch(FILE *fp);
void my_ungetch(int c);

void *get_lineno_list(struct lineno *, char[]);

void *gather_tree(struct tnode *, struct tlist *);

int compare(const void *, const void *);

struct nlist *install(char *, char *);
struct nlist *lookup(char *);
unsigned hash(char *);

void undef(char *s);

void print_nlist(int);

int define_processor();
int getterm(char *, int, FILE *);

#endif // include/chapter6_h_INCLUDED
