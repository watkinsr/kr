#ifndef __CHAPTER7_H_
#define __CHAPTER7_H_

#define MAXWORD 100
#define MAXLINE 1000
#define DEFAULT_PRECISION 100

#include <stdarg.h>
#include <stdio.h>

void casing(int argc, char **argv);
void minprintf(char *fmt, ...);
char *process_arg(char *, va_list);
void minscanf(
    char *fmt,
    ...);   /* minscanf: minimal scanf with variable argument list */


int cat(int argc, char *argv[]);
void filecopy(FILE *ifp, FILE *ofp);
int diff(char *, char *);

#endif // include/chapter7_h_INCLUDED

