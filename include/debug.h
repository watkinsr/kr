#include <stdio.h>

#define dprint(expr) printf(#expr " = %i\n", expr)
#define dprintd(expr) printf(#expr " = %g\n", expr)
#define dprints(expr) printf(#expr " = %s\n", expr)

void my_log(void *expr);
