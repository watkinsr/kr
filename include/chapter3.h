#ifndef __CHAPTER3_H_
#define __CHAPTER3_H_

void escape(char s[], char t[]);
int expand(char s1[], char s2[]);
void itoa(int n, char s[]);
void itoa2(int n, char s[], int field_width);
void itob(int n, char s[], int b);
void reverse(char s[]);

#endif // __CHAPTER3_H_
