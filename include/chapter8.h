#ifndef __CHAPTER8_H_
#define __CHAPTER8_H_

#define BUFSIZ  	1024
#define NULL 		0
#define EOF			(-1)
#define OPEN_MAX 	20

#define PERMS 0666

struct _flags {
    unsigned int _READ 	: 1;		/* file open for reading */
    unsigned int _WRITE : 1;		/* file open for writing */
    unsigned int _UNBUF : 1;		/* file is unbuffered */
    unsigned int _EOF   : 1;		/* EOF has occurred on this file */
    unsigned int _ERR   : 1;		/* error occured on this file */
} FLAGS;

enum _enum_flags {
    _READ 				= 01,
    _WRITE				= 02,
    _UNBUF 				= 04,
    _EOF				= 010,
    _ERR				= 020
};

typedef struct _iobuf {
    int cnt;									/* characters left */
    char *ptr;								/* next character position */
    char *base;								/* location of buffer */
    struct _flags flags;			/* mode of file access */
    int fd;										/* file descriptor */
} MY_FILE;

MY_FILE _my_iob[OPEN_MAX];

#define stdin 	(&_iob[0])
#define stdout	(&_iob[1])
#define stderr	(&_iob[2])

int _fillbuf(MY_FILE *);
int _flushbuf(int, MY_FILE *);

#define my_feof(p)		(((p)->flag == FLAGS._EOF) != 0)
#define my_ferror(p)	(((p)->flag == FLAGS._ERR) != 0)
#define my_fileno(p)	((p)->fd)

#define my_getc(p)		(--(p)->cnt >= 0 \
                     ? (unsigned char) *(p)->ptr++ : _fillbuf(p))
#define my_putc(x,p)		(--(p)->cnt >= 0 \
                     ? *(p)->ptr++ = (x) : _flushbuf((x),p))

#define my_getchar()		getc(stdin)
#define my_putchar(x)		putc((c), stdout)

int my_cat(int argc, char *argv[]);
void error(char *fmt, ...);

MY_FILE *my_fopen(char *name, char *mode);
int my_fflush(MY_FILE *);
int my_fclose(MY_FILE *fp);

int my_fseek(MY_FILE *fp, long offset, int origin);

void fsize(char *name);
void dirwalk(char *, void (*fcn)(char *));

#endif
