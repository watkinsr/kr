#ifndef __CHAPTER4_H_
#define __CHAPTER4_H_

#define MAXOP 100
#define NUMBER '0'
#define ERROR '1'
#define NEGATIVE_NUMBER '2'

#define SIN '3'
#define EXP '4'
#define POW '5'

#define VARIABLE '6'

#define MAXVAL 100
#define BUFSIZE 100

int strindex(char s[], char t[]);
double check_exp(char s[], int i, double val);
double atof2(char s[]);
int getop(char []);
int getop_by_line(char []);
void push(double);
double pop(void);
int getch(void);
int getch_line(void);
void ungetch(int);
void clear();
void ungets(char []);
void handle_op(int type, char s[]);

int calc_by_line();
int calc();

// Exercise 4-14
#define swap(t,x,y) t tmp = x; \
		    x = y; \
                    y = tmp;

#endif // __CHAPTER4_H_
