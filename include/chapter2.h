#ifndef __CHAPTER2_H_
#define __CHAPTER2_H_

void squeeze2(char s1[], char s2[]);
int any(char s1[], char s2[]);
unsigned getbits(unsigned x, int p, int n);
unsigned setbits(unsigned x, int p, int n, unsigned y);
unsigned invert(unsigned x, int p, int n);
/* unsigned rightrot(int x, int n); */
unsigned int rightrot(unsigned int x, unsigned int n);
int bitcount(unsigned x);

#endif // __CHAPTER2_H_
